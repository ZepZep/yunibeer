
namespace consts
{
    const uint8_t i2cAddr = 52<<1;
    const uint8_t masterDelay = 10;
    const uint8_t maxProtStep = 1.5 * masterDelay;
}

int8_t abs(int8_t a)
{
    if(a<0) return -a;
    else return a;
}
 
void welcome()
{
    display.print(" Hello! ");
    display.gotoXY(0, 1);
}

