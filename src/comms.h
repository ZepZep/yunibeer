//#include "lib/rs232.h"
#include <stdio.h>

#define BUFF_LEN 50

//-256 to 255 2x

class Comms
{
    char buffer[BUFF_LEN];
    uint8_t bufferLen;
    
public:
    Comms()
    {
        bufferLen = 0;
    }
    
    bool readIncomming(int8_t &motL, int8_t &motR)
    {
        char c;
        while(rs232.peek(c))
        {
            buffer[bufferLen++] = c;
            if(c == ';')
            {
                int a=0, b=0;
                sscanf(buffer, "%d %d", &a, &b);
                
                input2motors(a, b, motL, motR);
                
                bufferLen = 0;
                return true;
            }

            if(bufferLen > BUFF_LEN-1)
                bufferLen = 0;
        }
        
        return false;
    }
    
    void input2motors(int inA, int inB, int8_t &motL, int8_t &motR)
    {
        motL = inA;
        motR = inA;
    
        if(inB < 0)
            motL -= motL * (-inB) / 100;
        else
            motR -= motR * inB / 100;
    }
};
