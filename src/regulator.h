//#include "lib/motors.h"

class Regulator
{

public:
    Regulator()
    {
        lastProtL = 0;
        lastProtR = 0;
    }
    
    void setUserSpeeds(int16_t usrL, int16_t usrR)
    {
        
        
        
        
        
        setProtectedSpeeds(usrL, usrR);
    }
    
    void setProtectedSpeeds(int16_t l, int16_t r)
    {
        int16_t protL, protR;
        
        //Left
        int16_t deltaL = lastProtL - l;
        if(abs(deltaL) > consts::maxProtStep)
            protL = lastProtL - consts::maxProtStep * deltaL/abs(deltaL);
        else
            protL = l;
        
        //Right
        int16_t deltaR = lastProtR - r;
        if(abs(deltaR) > consts::maxProtStep)
            protR = lastProtR - consts::maxProtStep * deltaR/abs(deltaR);
        else
            protR = r;
        
        //for next use
        lastProtL = protL;
        lastProtR = protR;
        
        //set the motors
        setMotorPower(protL, protR);
    }
    
    void stop()
    {
        while(getMotorPowerID(0)!=0 or getMotorPowerID(1)!=0)
        {
            setProtectedSpeeds(0, 0);
            delay(consts::masterDelay);
        }
    }
    
    //filter settings
    void setP(double p)
    {
        kP = p;
    }
    void setI(double i)
    {
        kI = i;
    }
    void setD(double d)
    {
        kD = d;
    }
    void setPID(double p, double i; double d)
    {
        kP = p;
        kI = i;
        kD = d;
    }
    
private:
    int16_t lastProtL, lastProtR;
    
    int16_t err;
    int16_t kP, kI, kD;
    double sum, delta;
    
};
