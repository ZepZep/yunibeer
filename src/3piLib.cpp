#include "3piLib.h"

//acc+-gyro
//{
    //drivery
    //int gyro _ acc _ delta velocity
//}
//get errors
//{
    //prop - acc +- gyro
    //der - gyro
    //integ - SUM acc +- gyro
//}


//serial
//{
    // 10/s new velocity
        //wasd 00-FF;
    // 50/s error, tilt
    // >0.5 s without new velocity -> stop
//}

void run()
{
    welcome();
    
    //I2C init
    kubas::i2c.init();
    
    //rs232
    Comms rs;
    
    //regulator
    Regulator reg;
    int8_t usrInA = 0, usrInB = 0;
    int8_t usrVelL, usrVelR;
    
    uint32_t lastTicks = getTicksCount();
    
    //kubas::i2c.write(52<<1, test, 2);
    //kubas::i2c.wait();
    
    while(true)
    {
        uint32_t ticks = getTicksCount();
        uint32_t del = ticks - lastTicks;
        
        if(del > 1000)
        {
           reg.stop();  
        }
        
        if(rs.readIncomming(usrVelL, usrVelR))
        {
            lastTicks = getTicksCount();
            
            reg.setUserSpeeds(usrVelL, usrVelR);
        }
        
        delay(consts::masterDelay);
    }
}
