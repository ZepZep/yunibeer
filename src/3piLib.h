#ifndef JUNIOR_PI_LIB
#define JUNIOR_PI_LIB

#include "lib/common.h"
#include "lib/buzzer.h"
#include "lib/motors.h"
#include "lib/time.h"
#include "lib/sensors.h"
#include "lib/display.h"
#include "lib/rs232.h"
#include "lib/i2c.h"
#include "lib/buttons.h"
#include "lib/init.h"

#include "utils.h"
#include "comms.h"
#include "regulator.h"

#endif
